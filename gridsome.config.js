// This is where project configuration and installed plugin options are located.
// Learn more: https://gridsome.org/docs/config

module.exports = {
  siteName: 'Baptiste Marck',
  siteUrl: 'https://baptistemarck.be',
  siteDescription: 'Web designer & front-end developer',
  titleTemplate: '%s • Baptiste Marck',
  transformers: {
    remark: {}
  },
  plugins: [
    {
      use: "@gridsome/source-filesystem",
      options: {
        path: "project/**/*.md",
        typeName: "Project"
      }
    }
  ]
}
