---
label: "02"
title: "Nautilus"
image_teaser: "./images/nautilus_teaser.png"
image_screen: "./images/nautilus_screen.png"
image_background: "./images/nautilus_background.png"
website: "https://nautilusbyeaglestone.be"
year: 2017
slug: nautilus
---

### Roles
- Art direction
- Front-end developement

### Client
- Eaglestone

### Agency
- [Tilt Factory](https://tiltfactory.com)
