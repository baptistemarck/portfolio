import DefaultLayout from '~/layouts/Default.vue'
import Particles from '~/components/Particles.vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faCodepen } from '@fortawesome/fontawesome-free-brands'
import { faDribbble } from '@fortawesome/fontawesome-free-brands'
import { faDrupal } from '@fortawesome/fontawesome-free-brands'
import { faGithub } from '@fortawesome/fontawesome-free-brands'
import { faGulp } from '@fortawesome/fontawesome-free-brands'
import { faJs } from '@fortawesome/fontawesome-free-brands'
import { faLaravel } from '@fortawesome/fontawesome-free-brands'
import { faLinkedin } from '@fortawesome/fontawesome-free-brands'
import { faNpm } from '@fortawesome/fontawesome-free-brands'
import { faPhp } from '@fortawesome/fontawesome-free-brands'
import { faSass } from '@fortawesome/fontawesome-free-brands'
import { faShare } from '@fortawesome/free-solid-svg-icons'
import { faTwitter } from '@fortawesome/fontawesome-free-brands'
import { faVuejs } from '@fortawesome/fontawesome-free-brands'

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(
  faCodepen,
  faDribbble,
  faDrupal,
  faGithub,
  faGulp,
  faJs,
  faLaravel,
  faLinkedin,
  faNpm,
  faPhp,
  faSass,
  faShare,
  faTwitter,
  faVuejs
)

export default function (Vue) {
  Vue.component('Layout', DefaultLayout)
  Vue.component('Particles', Particles)
  Vue.component('font-awesome-icon', FontAwesomeIcon)
}
