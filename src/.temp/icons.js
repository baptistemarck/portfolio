export default {
  "touchiconMimeType": "image/png",
  "faviconMimeType": "image/png",
  "precomposed": false,
  "touchicons": [
    {
      "filename": "favicon.f22e9f3.ed7ace5.png",
      "src": "/assets/static/favicon.f22e9f3.ed7ace5.png",
      "width": 76,
      "height": 76
    },
    {
      "filename": "favicon.62d22cb.ed7ace5.png",
      "src": "/assets/static/favicon.62d22cb.ed7ace5.png",
      "width": 152,
      "height": 152
    },
    {
      "filename": "favicon.1539b60.ed7ace5.png",
      "src": "/assets/static/favicon.1539b60.ed7ace5.png",
      "width": 120,
      "height": 120
    },
    {
      "filename": "favicon.dc0cdc5.ed7ace5.png",
      "src": "/assets/static/favicon.dc0cdc5.ed7ace5.png",
      "width": 167,
      "height": 167
    },
    {
      "filename": "favicon.7b22250.ed7ace5.png",
      "src": "/assets/static/favicon.7b22250.ed7ace5.png",
      "width": 180,
      "height": 180
    }
  ],
  "favicons": [
    {
      "filename": "favicon.ce0531f.ed7ace5.png",
      "src": "/assets/static/favicon.ce0531f.ed7ace5.png",
      "width": 16,
      "height": 16
    },
    {
      "filename": "favicon.ac8d93a.ed7ace5.png",
      "src": "/assets/static/favicon.ac8d93a.ed7ace5.png",
      "width": 32,
      "height": 32
    },
    {
      "filename": "favicon.b9532cc.ed7ace5.png",
      "src": "/assets/static/favicon.b9532cc.ed7ace5.png",
      "width": 96,
      "height": 96
    }
  ]
}