export default {
  "siteUrl": "https://baptistemarck.be",
  "siteName": "Baptiste Marck",
  "titleTemplate": "%s • Baptiste Marck",
  "version": "0.4.7"
}