import NotFound from "/Users/bIDISTE/Sites/baptistemarck/node_modules/gridsome/app/pages/404.vue"

export const routes = [
  {
    name: "home",
    path: "/",
    component: () => import(/* webpackChunkName: "component--home" */ "~/pages/Index.vue"),
    meta: { data: true }
  },
  {
    name: "about",
    path: "/about",
    component: () => import(/* webpackChunkName: "component--about" */ "~/pages/About.vue")
  },
  {
    path: "/project/nautilus",
    component: () => import(/* webpackChunkName: "component--project" */ "~/templates/Project.vue"),
    meta: { data: true }
  },
  {
    path: "/project/trakks",
    component: () => import(/* webpackChunkName: "component--project" */ "~/templates/Project.vue"),
    meta: { data: true }
  }
]

export { NotFound }

