# baptistemarck

> A Vue.js project

## Build Setup

``` bash
# install dependencies
yarn install

# serve with hot reload at localhost:8080 & GraphQL Playground localhost:8080/___explore
gridsome develop

# build for production with minification
gridsome build

```

For a detailed explanation on how things work, check out the [docs for Gridsome](https://gridsome.org/docs/).
